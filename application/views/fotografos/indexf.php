<h1>Listado de Fotografos</h1>
<br>
<?php if ($fotografos): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>APELLIDO</th>
        <th>NOMBRE</th>
        <th>TELEFONO</th>
        <th>DIRECCION</th>
        <th>EMAIL</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($fotografos as $temporalF):?>
        <tr>
          <td>
            <?php echo $temporalF->id_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->cedula_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->apellido_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->nombre_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->telf_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->direccion_foto; ?>
          </td>
          <td>
            <?php echo $temporalF->email_foto; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Fotografo"><i class="glyphicon glyphicon-pencil"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url('/fotografos/eliminar/').$temporalF->id_foto;?>" title="Eliminar Fotografo"><i class="glyphicon glyphicon-trash" style="color: red;"></i></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h1>No hay Fotografos</h1>
<?php endif; ?>
<br>
