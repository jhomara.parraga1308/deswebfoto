<h1>Listado de Clientes</h1>
<br>
<?php if ($clientes): ?>
    <table class="table table-striped
    table-bordered">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>APELLIDO</th>
             <th>NOMBRE</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>EMAIL</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($clientes as $temporalC):?>
            <tr>
              <td>
                <?php echo $temporalC->id_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->cedula_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->apellido_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->nombre_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->telf_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->direccion_cli; ?>
              </td>
              <td>
                <?php echo $temporalC->email_cli; ?>
              </td>
              <td class="text-center">
                <a href="#" title="Editar Cliente"><i class="glyphicon glyphicon-pencil"></i></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="<?php echo site_url('/clientes/eliminar/').$temporalC->id_cli;?>" title="Eliminar cliente"><i class="glyphicon glyphicon-trash" style="color: red;"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
<?php else: ?>
  <h1>No hay Clientes</h1>
<?php endif; ?>
