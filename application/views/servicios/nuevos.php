<h1> ~ NUEVO SERVICIO ~</h1>
<br>
<br>
<form class=""
action="<?php echo site_url(); ?>/servicios/guardars"
method="post">

    <div class="row">
      <div class="col-sm-6">
          <label for="">Nombre :</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del servicio"
          class="form-control"
          name="nombre_ser" value=""
          id="nombre_ser">

          <label for="">Descripción:</label>
          <br>
          <input type="text"
          placeholder="Ingrese una pequeña descripción."
          class="form-control"
          name="descripcion_ser" value=""
          id="descripcion_ser">

          <label for="">Precio:</label>
          <br>
          <input type="number"
          placeholder="Ingresa su precio"
          class="form-control"
          name="precio_ser" value=""
          id="precio_ser">
      </div>
      <div class="col-sm-6">
            <img src="<?php echo base_url() ?>/assets/images/banner8.jpg" alt="Servicioss" width="650px">
      </div>
    </div>

    <br>
    <br>
    <div class="row">
        <div class="col-md-6 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/indexc"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
<br>
