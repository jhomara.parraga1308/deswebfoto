<h1>~ LISTADO DE SERICIOS ~</h1>
<br>
<?php if ($servicios): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCION</th>
        <th>PRECIO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($servicios as $temporalS):?>
        <tr>
          <td>
            <?php echo $temporalS->id_ser; ?>
          </td>
          <td>
            <?php echo $temporalS->nombre_ser; ?>
          </td>
          <td>
            <?php echo $temporalS->descripcion_ser; ?>
          </td>
          <td>
            <?php echo $temporalS->precio_ser; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Servicio"><i class="glyphicon glyphicon-pencil"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url('/servicios/eliminar/').$temporalS->id_ser;?>" title="Eliminar Servicio"><i class="glyphicon glyphicon-trash" style="color: red;"></i></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h1>No hay Fotografos</h1>
<?php endif; ?>
<br>
