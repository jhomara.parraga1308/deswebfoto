<?php

  class Clientes extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       $this->load->model('Cliente'); //para llamar ese mmodelo dentro del conssttructor
    }
    //funciones que reenderiza a vista index
    public function indexc()
    {
      $data['clientes']=$this->Cliente->obtenerTodos();
      $this->load->view('header');
      $this->load->view('clientes/indexc',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevoc()
    {
      $this->load->view('header');
      $this->load->view('clientes/nuevoc');
      $this->load->view('footer');
    }

    public function guardarc(){
      $datosNuevoCliente=array(
        "cedula_cli"=>$this->input->post('cedula_cli'),
        "apellido_cli"=>$this->input->post('apellido_cli'),
        "nombre_cli"=>$this->input->post('nombre_cli'),
        "telf_cli"=>$this->input->post('telf_cli'),
        "direccion_cli"=>$this->input->post('direccion_cli'),
        "email_cli"=>$this->input->post('email_cli')
      );
      if ($this->Cliente->
      insertarc($datosNuevoCliente)){
        redirect(site_url());
      }else {
          echo "error al insertar los clientes";
      }
        // code...
    }
    public function eliminar($id_cli){
            //Funcion para eliminar datos (instructores)
            //echo $id_ins;
            if ($this->Cliente->borrar($id_cli)) {
                redirect('clientes/indexc');
            } else {
                echo "ERROR AL BORRAR :(";
            }
        }

  }// cierre de la clase


 ?>
