<?php

  class Servicios extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       $this->load->model('Servicio'); //para llamar ese mmodelo dentro del conssttructor
    }
    //funciones que reenderiza a vista index
    public function indexs()
    {
      $data['servicios']=$this->Servicio->obtenerTodos();
      $this->load->view('header');
      $this->load->view('servicios/indexs',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevos()
    {
      $this->load->view('header');
      $this->load->view('servicios/nuevos');
      $this->load->view('footer');
    }

    public function guardars(){
      $datosNuevoServicio=array(
        "nombre_ser"=>$this->input->post('nombre_ser'),
        "descripcion_ser"=>$this->input->post('descripcion_ser'),
        "precio_ser"=>$this->input->post('precio_ser')
      );
      if ($this->Servicio->
      insertars($datosNuevoServicio)){
        redirect(site_url());
      }else {
          echo "error al insertar los servicios";
      }
        // code...
    }
    public function eliminar($id_ser){
            //Funcion para eliminar datos (instructores)
            //echo $id_ins;
            if ($this->Cliente->borrar($id_ser)) {
                redirect('servicios/indexs');
            } else {
                echo "ERROR AL BORRAR :(";
            }
        }

  }// cierre de la clase


 ?>
