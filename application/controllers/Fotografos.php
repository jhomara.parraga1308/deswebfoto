<?php

  class Fotografos extends CI_Controller
  {

    function __construct()
    {
       parent::__construct();
       $this->load->model('Fotografo'); //para llamar ese mmodelo dentro del conssttructor
    }
    //funciones que reenderiza a vista index
    public function indexf() //listar
    {
      $data['fotografos']=$this->Fotografo->obtenerTodos();
      $this->load->view('header');
      $this->load->view('fotografos/indexf',$data);
      $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevof() //registro
    {
      $this->load->view('header');
      $this->load->view('fotografos/nuevof');
      $this->load->view('footer');
    }

    public function guardarf(){ //registrar
      $datosNuevoFotografo=array(
        "cedula_foto"=>$this->input->post('cedula_foto'),
        "apellido_foto"=>$this->input->post('apellido_foto'),
        "nombre_foto"=>$this->input->post('nombre_foto'),
        "telf_foto"=>$this->input->post('telf_foto'),
        "direccion_foto"=>$this->input->post('direccion_foto'),
        "email_foto"=>$this->input->post('email_foto')
      );
      if ($this->Fotografo->insertarf($datosNuevoFotografo)){
        redirect(site_url());
      }else{
        echo "Error al insertar un nuevo fotografo";
      }
    }
    public function eliminar($id_foto){
            //echo $id_mas;
            if ($this->Fotografo->borrar($id_foto)) {
                redirect('fotografos/indexf');
            } else {
                echo "ERROR AL BORRAR :(";
            }

        }
  }


 ?>
