<?php
  class Servicio extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertars($datos){
        return $this->db
                ->insert("Servicio",
                $datos);
    }

    function obtenerTodos(){
      $listadoServicios=
      $this->db->get("Servicio");
      if($listadoServicios->num_rows()>0){ //si hay datos return los datos que hay
        return $listadoServicios->result();
      }else{//no hay datos return false
        return false;
      }
    }
    public function borrar($id_ser){
            $this->db->where("id_ser", $id_ser);
            return $this->db->delete('Servicio');
        }
  }//Cierre de la clase

 ?>
