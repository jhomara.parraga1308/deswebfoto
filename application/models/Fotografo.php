<?php
  class Fotografo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un fotografo en MYSQL
    function insertarf($datos){
        return $this->db
                ->insert("Fotografo",
                $datos);
    }

    function obtenerTodos(){ //listar
      $listadoFotografos=
      $this->db->get('Fotografo');
      if ($listadoFotografos->num_rows()>0) { //Usamos esta condicion para que cuando haya datos, retorne los que haya
        return $listadoFotografos->result();
      }else{
        return false;
      }
    }
    public function borrar($id_foto){
            $this->db->where("id_foto", $id_foto);
            return $this->db->delete('Fotografo');
        }

  }//Cierre de la clase

 ?>
