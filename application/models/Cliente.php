<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertarc($datos){
        return $this->db
                ->insert("Cliente",
                $datos);
    }

    function obtenerTodos(){
      $listadoClientes=
      $this->db->get("Cliente");
      if($listadoClientes->num_rows()>0){ //si hay datos return los datos que hay
        return $listadoClientes->result();
      }else{//no hay datos return false
        return false;
      }
    }
    public function borrar($id_cli){
            $this->db->where("id_cli", $id_cli);
            return $this->db->delete('Cliente');
        }
  }//Cierre de la clase

 ?>
